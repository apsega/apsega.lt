---
title: "computing.co.uk: Bare metal Kubernetes at Adform: 'We like it and the developers love it'"
date: 2018-05-22T10:10:00+00:00
draft: false
categories: 
  - "articles"
---
## Bare metal Kubernetes at Adform: 'We like it and the developers love it'

Migrating to containers has meant 20 per cent performance improvement and happier devs, say systems engineers at the adtech firm. Full article - [https://www.computing.co.uk/analysis/3032724/bare-metal-kubernetes-at-adform-we-like-it-and-the-developers-love-it](https://www.computing.co.uk/analysis/3032724/bare-metal-kubernetes-at-adform-we-like-it-and-the-developers-love-it)