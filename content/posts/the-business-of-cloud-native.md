---
title: "The Business of Cloud Native: The Kubernetes Learning Curve with Edgaras Apsega"
date: 2020-05-27T10:00:00+00:00
draft: false
categories: 
  - "podcasts"
---

## The Business of Cloud Native: The Kubernetes Learning Curve with Edgaras Apsega"

In the inaugural episode of The Business of Cloud Native, host Emily Omier interviews Edgaras Apsega of Adform about his journey into the world of Kubernetes. They discuss the reasons for switching to a cloud native environment, the approval process, and the exponential growth Adform has experienced since making the switch. There have been growing pains organizationally and technically, however. Despite these surprises, Edgaras is pleased with the results so far and looks forward to continuing on the cloud native journey.

Link to podcast - [https://share.transistor.fm/s/edfdea21](https://share.transistor.fm/s/edfdea21)

