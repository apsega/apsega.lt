---
title: "vinted.engineering: Intrusion detection for containers"
date: 2023-08-31T10:10:00+00:00
draft: false
categories: 
  - "articles"
---
## Intrusion detection for containers'

Experience with Falco, an open-source tool designed specifically for securing containerised environments.. Full article - [https://vinted.engineering/2023/08/31/intrusion-detection-for-containers/](https://vinted.engineering/2023/08/31/intrusion-detection-for-containers/)