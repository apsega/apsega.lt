---
title: "Certified Kubernetes Administrator (CKA)"
date: 2020-11-21T10:00:00+00:00
draft: false
categories: 
  - "certifications"
---

## Certified Kubernetes Administrator (CKA)

![CKA Badge](https://images.youracclaim.com/size/340x340/images/8b8ed108-e77d-4396-ac59-2504583b9d54/cka_from_cncfsite__281_29.png)

[https://www.youracclaim.com/badges/8e76d681-eb62-4fb7-bd9d-341604e9ae07](https://www.youracclaim.com/badges/8e76d681-eb62-4fb7-bd9d-341604e9ae07)
