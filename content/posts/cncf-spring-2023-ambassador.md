---
title: "CNCF Spring 2023 Ambassador"
date: 2023-03-22T10:00:00+00:00
draft: false
categories:
  - "certifications"
---

## CNCF Spring 2023 Ambassador

![Member Badge](https://images.credly.com/images/faa0c895-889a-4ef0-a2fb-516e0abe9c97/image.png)

[https://www.credly.com/badges/fc072bca-adf7-4863-ae9f-6e2e5039bb32/public_url](https://www.credly.com/badges/fc072bca-adf7-4863-ae9f-6e2e5039bb32/public_url)
