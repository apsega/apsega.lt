---
title: "Certified Kubernetes Security Specialist (CKS)"
date: 2024-04-17T10:00:00+00:00
draft: false
categories: 
  - "certifications"
---

## Certified Kubernetes Security Specialist (CKS)


![CKS Badge](https://images.credly.com/images/9945dfcb-1cca-4529-85e6-db1be3782210/kubernetes-security-specialist-logo2.png)

[https://www.credly.com/badges/2d7203d0-734e-45f0-8ac6-08fba0add3e0/public_url](https://www.credly.com/badges/2d7203d0-734e-45f0-8ac6-08fba0add3e0/public_url)
