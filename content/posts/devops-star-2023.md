---
title: "DevOps Star of The Year (2023)"
date: 2023-11-15T10:00:00+00:00
draft: false
categories: 
  - "awards"
---

## Build Stuff Tech Awards 2023: DevOps Star of The Year

![Award](https://static.wixstatic.com/media/4df62a_9e55b99d431b43ec8f3f3a118b032467~mv2.jpg/v1/fill/w_1058,h_706,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/WTC-04640.jpg)

[https://www.buildstuff.events/awards](https://www.buildstuff.events/awards)
