---
title: "ArgoCon Europe 2023 Program Committee Member"
date: 2023-03-22T10:00:00+00:00
draft: false
categories:
  - "certifications"
---

## ArgoCon Europe 2023 Program Committee Member

![Member Badge](https://images.credly.com/images/da4d889d-7909-44cb-89b2-40cba1e11881/image.png)

[https://www.credly.com/badges/055731d2-74dd-40be-8749-a6bdaf0df19c/public_url](https://www.credly.com/badges/055731d2-74dd-40be-8749-a6bdaf0df19c/public_url)
