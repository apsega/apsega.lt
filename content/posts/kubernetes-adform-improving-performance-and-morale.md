---
title: "kubernetes.io: Adform: Improving Performance and Morale with Cloud Native"
date: 2018-05-29T10:00:00+00:00
draft: false
affiliatelink: "https://kubernetes.io/case-studies/adform/"
categories: 
  - "articles"
---

## Adform: Improving Performance and Morale with Cloud Native

"Kubernetes enabled the self-healing and immutable infrastructure. We can do faster releases, so our developers are really happy. They can ship our features faster than before, and that makes our clients happier."

Full article - [https://kubernetes.io/case-studies/adform/](https://kubernetes.io/case-studies/adform/)