---
title: "Kubestronaut"
date: 2024-04-22T10:00:00+00:00
draft: false
categories: 
  - "awards"
---

## Kubestronaut

Kubestronauts are people who showed exemplary Kubernetes knowledge by passing all five of CNCF’s Kubernetes-related certifications: Certified Kubernetes Administrator (CKA), Certified Kubernetes Application Developer (CKAD), Certified Kubernetes Security Specialist (CKS), Kubernetes and Cloud Native Associate (KCNA) and Kubernetes and Cloud Security Associate (KCSA).

**Earning Criteria**

- Credential: Obtained all five of CNCF’s Kubernetes-related certifications.
- Education Experience: Showed exemplary Kubernetes knowledge and skills.

![Kubestronaut Badge](https://images.credly.com/images/cd6c6449-6814-4613-a2d3-13cf4ac5be4f/image.png)

[https://www.credly.com/badges/3b3aacb8-0aca-41e3-b0f3-16530b1924f8/public_url](https://www.credly.com/badges/3b3aacb8-0aca-41e3-b0f3-16530b1924f8/public_url)
