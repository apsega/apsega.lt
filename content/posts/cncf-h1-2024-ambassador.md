---
title: "CNCF Ambassador H1 2024"
date: 2024-03-30T10:00:00+00:00
draft: false
categories:
  - "certifications"
---

## CNCF Ambassador H1 2024

![Member Badge](https://images.credly.com/images/5302cfb2-cecd-410f-8a23-7a29a9f11ccb/image.png)

[https://www.credly.com/badges/fc072bca-adf7-4863-ae9f-6e2e5039bb32/public_url](https://www.credly.com/badges/c4bc8b40-0610-4067-aeac-7f255a3e5834/public_url)
