---
title: "Secure Kubernetes Software Supply Chain using SLSA Framework"
date: 2023-11-10T10:00:00+00:00
draft: false
categories: 
  - "presentations"
---

## Secure Kubernetes Software Supply Chain using SLSA Framework

[https://www.nordicdevopsdays.com](https://www.nordicdevopsdays.com)

[https://speakerdeck.com/apsega/securing-kubernetes-software-supply-chain-using-slsa-framework](https://speakerdeck.com/apsega/securing-kubernetes-software-supply-chain-using-slsa-framework)

