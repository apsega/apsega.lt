---
title: "cncf.io: Adform: Improving Performance and Morale with Cloud Native"
date: 2018-05-29T10:00:00+00:00
draft: false
affiliatelink: "https://www.cncf.io/case-study/adform/"
categories: 
  - "articles"
---

## Adform: Improving Performance and Morale with Cloud Native

Adform has a large infrastructure: OpenStack-based private clouds running on 1,100 physical servers in 7 data centers around the world, 3 of which were opened in the past year. With the company’s growth, “our private cloud was not really flexible enough,” says IT System Engineer Edgaras Apšega. “The biggest pain point is that our developers need to maintain their virtual machines, so rolling out technology and new software takes time. We were really struggling with our releases, and we didn’t have self-healing infrastructure.”

Full article - [https://www.cncf.io/case-study/adform/](https://www.cncf.io/case-study/adform/)

{{< youtube JuRpg6F-nvM >}}