---
title: "Lessons learned using GitOps to deploy thousands of Kubernetes pods"
date: 2022-11-17T10:00:00+00:00
draft: false
categories: 
  - "presentations"
---

## Lessons learned using GitOps to deploy thousands of Kubernetes pods

[https://www.nordicdevopsdays.com](https://www.nordicdevopsdays.com)

[https://speakerdeck.com/apsega/lessons-learned-using-gitops-to-deploy-thousands-of-kubernetes-pods](https://speakerdeck.com/apsega/lessons-learned-using-gitops-to-deploy-thousands-of-kubernetes-pods)

