---
title: "Increased velocity of code deployments with Kubernetes"
date: 2023-11-10T10:00:00+00:00
draft: false
categories: 
  - "presentations"
---

## Increased velocity of code deployments with Kubernetes

[http://buildstuff.events](http://buildstuff.events)

[https://speakerdeck.com/apsega/increased-velocity-of-code-deployments-with-kubernetes](https://speakerdeck.com/apsega/increased-velocity-of-code-deployments-with-kubernetes)

