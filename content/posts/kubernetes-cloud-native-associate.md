---
title: "Kubernetes and Cloud Native Associate (KCNA)"
date: 2024-04-03T10:00:00+00:00
draft: false
categories: 
  - "certifications"
---

## Kubernetes and Cloud Native Associate (KCNA)

![KCNA Badge](https://images.credly.com/images/f28f1d88-428a-47f6-95b5-7da1dd6c1000/KCNA_badge.png)

[https://www.credly.com/badges/5651a23a-efc5-49af-94e0-d1625281a691/public_url](https://www.credly.com/badges/5651a23a-efc5-49af-94e0-d1625281a691/public_url)
