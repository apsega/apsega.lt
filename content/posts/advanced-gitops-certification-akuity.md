---
title: "Advanced GitOps with Akuity"
date: 2022-09-26T10:00:00+00:00
draft: false
categories:
  - "certifications"
---

## Advanced GitOps with Akuity

![GitOps Badge](https://api.accredible.com/v1/frontend/credential_website_embed_image/badge/59042307)

[https://www.credential.net/ea7f2ef1-a578-48d4-88f1-513a9c7a1a87](https://www.credential.net/ea7f2ef1-a578-48d4-88f1-513a9c7a1a87)


