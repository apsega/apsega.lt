---
title: "Certified Kubernetes Application Developer (CKAD)"
date: 2024-03-25T10:00:00+00:00
draft: false
categories: 
  - "certifications"
---

## Certified Kubernetes Application Developer (CKAD)

![CKAD Badge](https://images.credly.com/images/f88d800c-5261-45c6-9515-0458e31c3e16/ckad_from_cncfsite.png)

[https://www.credly.com/badges/911d9348-8ed2-4ce1-a468-f933650a1780/public_url](https://www.credly.com/badges/911d9348-8ed2-4ce1-a468-f933650a1780/public_url)
