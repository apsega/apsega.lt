---
title: "Falco in action: protecting your Cloud Native infrastructure"
date: 2024-05-18T10:00:00+00:00
draft: false
categories: 
  - "presentations"
---

## Falco in action: protecting your Cloud Native infrastructure

[https://www.devopsinshorts.com](https://www.devopsinshorts.com)

[https://speakerdeck.com/apsega/falco-in-action-protecting-your-cloud-native-infrastructure](https://speakerdeck.com/apsega/falco-in-action-protecting-your-cloud-native-infrastructure)

